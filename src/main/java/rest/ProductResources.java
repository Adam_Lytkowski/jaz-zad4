package rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import domain.Comment;
import domain.Product;
import domain.services.ProductService;

@Path("/product")
@Stateless
public class ProductResources {
private ProductService ps = new ProductService();
	
	@PersistenceContext
	EntityManager em;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getAll() {
		return em.createNamedQuery("product.all", Product.class).getResultList();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Add(Product product) {
		em.persist(product);
		return Response.ok(product.getId()).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") int id){
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("productId", id)
				.getSingleResult();
		if(result==null){
			return Response.status(404).build();
		}
		return Response.ok(result).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") int id, Product p){
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("productId", id)
				.getSingleResult();
		if(result==null){
			return Response.status(404).build();
		}
		result.setName(p.getName());
		result.setPrice(p.getPrice());
		result.setCategory(p.getCategory());
		em.persist(result);
		return Response.ok().build();
	}
	
	@POST
	@Path("/search/name")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductsByName(Product product){
		List<Product> products = new ArrayList<Product>();
		List<Product> result = new ArrayList<Product>();
		products = em.createNamedQuery("product.all", Product.class).getResultList();
		
		for(Product p : products){
			if(p.getName().contains(product.getName()))
				result.add(p);
		}
		
		return result;
	}
	
	@POST
	@Path("/search/category")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductsByCategory(Product product){
		List<Product> products = new ArrayList<Product>();
		List<Product> result = new ArrayList<Product>();
		products = em.createNamedQuery("product.all", Product.class).getResultList();
		
		for(Product p : products){
			if(p.getCategory().equals(product.getCategory()))
				result.add(p);
		}
		
		return result;
	}
	
	@GET
	@Path("/search/price/{min}/{max}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductsByPrice(@PathParam("min") int min, @PathParam("max") int max){
		List<Product> products = new ArrayList<Product>();
		List<Product> result = new ArrayList<Product>();
		products = em.createNamedQuery("product.all", Product.class).getResultList();
		
		for(Product p : products){
			if(p.getPrice()>min && p.getPrice()<max)
				result.add(p);
		}
		
		return result;
	}
	
	@GET
	@Path("/{productId}/comments")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getComments(@PathParam("productId") int productId){
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("productId", productId)
				.getSingleResult();
		if(result==null)
			return null;
		return result.getComments();
	}
	
	@POST
	@Path("/{productId}/comments")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addComment(@PathParam("productId") int productId, Comment comment){
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("productId", productId)
				.getSingleResult();
		if(result==null)
			return Response.status(404).build();
		result.getComments().add(comment);
		comment.setProduct(result);
		em.persist(comment);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/{productId}/comments/{commentId}")
	public Response deleteComment(@PathParam("productId") int productId,@PathParam("commentId") int commentId){
				
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("productId", productId)
				.getSingleResult();
		if(result==null)
			return Response.status(404).build();
		
		
		em.remove(result.getComments().get(commentId));
		result.getComments().remove(commentId);
		
	    return Response.ok().build();
	}
	
}
